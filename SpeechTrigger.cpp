#include <voce.h>
#include <iostream>
#include <string>
#include <sstream>

#ifdef WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

// Send On/Off message to DepthTracking Module
void DepthAlertTrigger(bool b)
{
	HWND hwnd = FindWindow(NULL, "Depth Tracking");
	if (hwnd != 0)
	{
		char* str = "DepthModuleOn";
		if (b == false)
		{
			str = "DepthModuleOff";
		}
		COPYDATASTRUCT cd;
		cd.dwData = 100;
		cd.cbData = strlen(str) + 1;
		cd.lpData = str;
		SendMessage(hwnd, WM_COPYDATA, 0, reinterpret_cast< LPARAM >(&cd));
	}
}

// Start an exe file
// Reference: http://stackoverflow.com/a/15440094
VOID startup(LPCTSTR lpApplicationName)
{
	// additional information
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	// set the size of the structures
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	// start the program up
	CreateProcess(lpApplicationName,   // the path
		NULL,        // Command line
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		0,              // No creation flags
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory 
		&si,            // Pointer to STARTUPINFO structure
		&pi           // Pointer to PROCESS_INFORMATION structure
	);
	// Close process and thread handles. 
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
}

int main(int argc, char **argv)
{
	//Initiate voce voice recognition
	voce::init("./lib", true, true, "./grammar", "depth");
	std::cout << "Speak 'Command run depth alert' or 'Command stop depth alert'.";
	std::cout << "\n";

	// Run Depth Tracking Module
	startup("D:\\Dropbox\\kinect-depth-tracking\\Release\\DepthTracking.exe");
	//startup("DepthTracking.exe");

	voce::synthesize("Depth alert module started.");

	//Keeps the recognition active
	bool quit = false;
	while (!quit)
	{
			while (voce::getRecognizerQueueSize() > 0)
			{
				std::string s = voce::popRecognizedString();

				std::cout << s;
				std::cout << "\n";

				if (std::string::npos != s.rfind("command run depth alert")){
					DepthAlertTrigger(true);
					voce::synthesize("Start.");
				}
				if (std::string::npos != s.rfind("command stop depth alert")){
					DepthAlertTrigger(false);
					voce::synthesize("Stop.");
				}
			}
	}
	return 0;
}